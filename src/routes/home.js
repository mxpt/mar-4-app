import React, { useState, useEffect } from 'react'

function Home() {
  const [state, setState] = useState(null)
  const [selectedCategory, setSelectedCategory] = useState(null)
  const [search, setSearch] = useState('')

  useEffect(() => {
    fetch('https://api.mocki.io/v1/47ed1580')
      .then((res) => res.json())
      .then((data) => {
        const newState = data.categories.reduce((acc, cur) => {
          const {
            id,
            name,
            parent_id,
            title,
          } = cur
          if (!parent_id) {
            acc.push({
              id,
              name,
              items: [],
            })
          } else {
            acc.find((category) => category.id === parent_id).items.push({
              id,
              name,
              title,
            })
          }
          return acc
        }, [])

        setState(newState)
      })
  }, [])

  if (!state) {
    return null
  }

  const currentCategory = state.find((category) => category.id === selectedCategory)
    || { items: [] }
  const items = currentCategory.items
    .filter(({ name }) => {
      if (name.toLowerCase().includes(search.toLowerCase())) {
        return true
      }
      return false
    })
    .reduce((acc, cur) => {
      const {
        title = 'Lainnya',
      } = cur
      if (!acc[title]) {
        acc[title] = [cur]
      } else {
        acc[title].push(cur)
      }
      return acc
    }, {})

  return (
    <div>
      <div className="searchbox">
        <input type="text" value={search} onChange={e => setSearch(e.target.value)} />
      </div>
      {
        state && (
          <div className="grid">
            <div>
              {
                state.map((category) => (
                  <div
                    className="cat"
                    onClick={() => setSelectedCategory(category.id)}
                    key={category.id}
                  >
                    {category.name}
                  </div>
                ))
              }
            </div>
            <div>
              {
                items && (
                  Object.entries(items)
                    .sort((a) => a[0] === 'Lainnya')
                    .map(([subcategory, subcategoryItems]) => (
                      <div className="subcat" key={subcategory}>
                        <b>{subcategory}</b>
                        <div>
                          {
                            subcategoryItems.map(({ id, name }) => (
                              <div key={id}>
                                {name}
                              </div>
                            ))
                          }
                        </div>
                      </div>
                    ))
                )
              }
              {
                !Object.keys(items).length && (
                  <div>
                    {selectedCategory ? 'Kosong' : 'Pilih kategori'}
                  </div>
                )
              }
            </div>
          </div>
        )
      }
    </div>
  )
}

export default Home
